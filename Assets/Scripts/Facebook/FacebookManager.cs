﻿using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FacebookManager : MonoBehaviour
{
    private Action<Facebook.Unity.AccessToken, string, string> Connected;
    private Facebook.Unity.AccessToken CurrentAccessToken;

    public void Awake()
    {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }   
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    /*public void ConnectoToFB()
    {
        FB.ActivateApp();
        Debug.Log("Logging Into Facebook...");

        //Connected = action;
        var perms = new List<string>() { "public_profile", "email" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }*/

    public void ConnectoToFB(Action<Facebook.Unity.AccessToken, string, string> action)
    {
        FB.ActivateApp();
        Debug.Log("Logging Into Facebook...");

        Connected = action;
        var perms = new List<string>() { "public_profile", "email" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }

    private void AuthCallback(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            CurrentAccessToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            // Print current access token's User ID
            Debug.Log(CurrentAccessToken.UserId);
            // Print current access token's granted permissions
            foreach (string perm in CurrentAccessToken.Permissions)
            {
                Debug.Log(perm);
            }
            //FB.API("me/picture?height=256&width=256", HttpMethod.GET, FbGetPicture);
            FB.API("me/?fields=first_name,last_name", HttpMethod.GET, DealWithProfile);
        }
        else
        {
            Debug.Log("User cancelled login");
            Connected(null, null, null);
        }
    }

    private void DealWithProfile(IGraphResult result)
    {
        Debug.Log("Callback GraphResult");
        if (string.IsNullOrEmpty(result.Error))
        {
            Connected(CurrentAccessToken, Convert.ToString(result.ResultDictionary["first_name"]), Convert.ToString(result.ResultDictionary["last_name"]));
        }
        else
        {
            Debug.Log("received error=" + result.Error);
        }
    }

    public IEnumerator GetFBPicture(string facebookId, Action<Sprite> callback)
    {

        using (var www = new UnityWebRequest("https://graph.facebook.com/" + facebookId + "/picture?type=large"))
        {
            DownloadHandlerTexture texDl = new DownloadHandlerTexture(true);
            www.downloadHandler = texDl;
            yield return www.SendWebRequest();

            if (www.isHttpError || www.isNetworkError)
            {
                callback(null);
            }
            else if (www.isDone)
            {
                Texture2D tempPic = texDl.texture;
                callback(Sprite.Create(tempPic, new Rect(0, 0, texDl.texture.width, texDl.texture.height), new Vector2()));
            }
        }
    }




}
