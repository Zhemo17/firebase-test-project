﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using Firebase.Auth;

[RequireComponent(typeof(FacebookManager))]
[RequireComponent(typeof(AppleManager))]
[RequireComponent(typeof(GoogleManager))]
public class FirebaseAuth : MonoBehaviour
{
    FirebaseManager FirebaseManager => GetComponent<FirebaseManager>();
    FacebookManager FacebookManager => GetComponent<FacebookManager>();
    AppleManager AppleManager => GetComponent<AppleManager>();
    GoogleManager GoogleManager => GetComponent<GoogleManager>();


    string googleIdToken;
    string googleAccessToken;

    private Firebase.Auth.FirebaseAuth auth { get; set; }
    private Credential credential { get; set; }
    public Firebase.Auth.FirebaseUser user { get; private set; }

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void ClickFacebookLogin()
    {
        FacebookManager.ConnectoToFB(
               (token, firstName, lastName) =>
               {
                   if (token == null)
                   {

                   }
                   else
                   {
                       //loadingIcon.SetActive(true);
                       StartCoroutine(GetFBSign(OnRegisterLogin, () => { /*loadingIcon.SetActive(false);*/ }, token, firstName, lastName));
                   }
               });

        //FacebookManager.ConnectoToFB();
    }


    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            
            if (!signedIn && user != null)
            {
                Debug.Log("Signed out " + user.UserId);
            }

            user = auth.CurrentUser;
            if (user != null)
            {
                string name = user.DisplayName;
                string email = user.Email;
                Uri photo_url = user.PhotoUrl;
                // The user's Id, unique to the Firebase project.
                // Do NOT use this value to authenticate with your backend server, if you
                // have one; use User.TokenAsync() instead.
                string uid = user.UserId;
            }
        }
    }


    public void Init()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
       
        auth.StateChanged += AuthStateChanged;

        AuthStateChanged(this, null);
    }


    public void GetGoogleSign()
    {
        Credential credential = GoogleAuthProvider.GetCredential(googleIdToken, googleAccessToken);

        Sign();
    }


    public void GetAppleSign()
    {
        //TODO set custom token
        SignCustom("");
    }

    public IEnumerator GetFBSign(Action<string, AuthError> action, Action fallback, AccessToken accessToken, string firstName, string lastName)
    {
        credential = FacebookAuthProvider.GetCredential(accessToken.TokenString);

        Sign();

        yield return null;
    }

    /*public IEnumerator GetFBSign(Action<string, AuthError> action, Action fallback, AccessToken accessToken, string firstName, string lastName)
    {
        credential = FacebookAuthProvider.GetCredential(accessToken.TokenString);

        Sign();

        yield return null;
    }*/


    void Sign()
    {
        auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithCredentialAsync was canceled.");
                return;
            }
            
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                return;
            }

            FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
        });
    }


    void SignCustom(string custom_token)
    {
        auth.SignInWithCustomTokenAsync(custom_token).ContinueWith(task => {
            
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithCustomTokenAsync was canceled.");
                return;
            }
            
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithCustomTokenAsync encountered an error: " + task.Exception);
                return;
            }

            FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
        });
    }
 

    public void SignOut()
    {
        auth.SignOut();
    }


    public void OnDestroy()
    {
        if (auth != null)
            auth.StateChanged -= AuthStateChanged;
        
        auth = null;
    }

    #region CALLBACKS
    public void OnRegisterLogin(string message, AuthError authError)
    {
        Debug.Log("OnRegisterLogin is " + authError);
        if (authError == 0)
        {
            //FreshLogin();
        }
        else
        {
            Debug.Log("Register Error : "+  message);
           /* InGameMessagesController.Instance.ShowPopup(InGameMessageIcon.Error, "Register Error", message, "Ok",
                () => {
                    //loadingIcon.SetActive(false);
                });*/
        }
    }

    #endregion

}
