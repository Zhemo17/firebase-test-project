﻿using System;
using UnityEngine;
using Firebase;

[RequireComponent(typeof(FirebaseAuth))]
[RequireComponent(typeof(FirebaseRemoteConfig))]
public class FirebaseManager : MonoBehaviour
{
    Firebase.FirebaseApp app;
    public bool isReady { get; private set;}

    public FirebaseAuth Auth => GetComponent<FirebaseAuth>();
    public FirebaseRemoteConfig RemoteConfig => GetComponent<FirebaseRemoteConfig>();

    private void Awake()
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    private void Init()
    {
        Auth.Init();

        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {

            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                app = Firebase.FirebaseApp.DefaultInstance;

                //Auth.Init();
                // Set a flag here to indicate whether Firebase is ready to use by your app.
                isReady = true;
            }
            else
            {
                Debug.LogError(String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
    }

}
